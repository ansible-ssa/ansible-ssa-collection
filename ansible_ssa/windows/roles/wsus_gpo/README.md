# Ansible Role: wsus_gpo

This role will help to create a Windows Server Update Services Group Policy Object.

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
  - name: azure.azcollection
```

If you want to use this role in your playbook:

```yaml
- name: Create WSUS GPO
  var:
    wsus_gpo_name: "WSUS – Auto Updates and Intranet Update Service Location"
    wsus_loc_gpo_reg_path: HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate
    wsus_DoNotConnectToWindowsUpdateInternetLocations: 0
    wsus_au_gpo_reg_path: HKLM\Software\Policies\Microsoft\Windows\WindowsUpdate\AU
    wsus_AUOptions: 3
    wsus_NoAutoUpdate: 1
    wsus_RebootWarningTimeoutEnabled: 1
    wsus_ou_containers: 
      - OU=Domain Controllers,DC=redhat,DC=local
      - OU=WindowsServers,DC=redhat,DC=local
  ansible.builtin.include_role:
    name: ansible_ssa.windows.wsus_gpo
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
