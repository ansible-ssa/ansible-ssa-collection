# Ansible Role: wsus_pull_report

This role will help to pull a report from a Windows Server Update Services

## How to install this role

The Ansible SSA Windows collection containing this role is shipped with the Ansible SSA Execution Environment. For best results, it is recommended to use the Execution Environment to make sure all prerequisites are met.

## How to use this role

If you want to use this role, you'll need the following collections un your requirements file:

```yaml
---
collections:
  - name: ansible.windows
  - name: community.windows
  - name: ansible_ssa.windows
  - name: community.general
```

If you want to use this role in your playbook:

```yaml
- name: Pull report from WSUS
  var:
    wsus_content_dir: C:\WSUS
  ansible.builtin.include_role:
    name: ansible_ssa.windows.wsus_pull_report
```

## Variables

Check the [defaults/main.yml](./defaults/main.yml) for details on variables used by this role.
